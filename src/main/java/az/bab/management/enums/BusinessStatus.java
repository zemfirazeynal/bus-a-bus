package az.bab.management.enums;

import lombok.Getter;

@Getter
public enum BusinessStatus {

    SUCCESS(0, "Successfully process"),
    ERROR(1001, "RuntimeException process");


    private int code;
    private String message;

    BusinessStatus(int code, String message) {
        this.code = code;
        this.message = message;
    }


}
