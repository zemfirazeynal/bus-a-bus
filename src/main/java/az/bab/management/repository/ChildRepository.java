package az.bab.management.repository;

import az.bab.management.domain.ChildEntity;
import az.bab.management.domain.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ChildRepository extends JpaRepository<ChildEntity, Long> {

    Optional<ChildEntity> findByIdAndUser(Long id, UserEntity user);


    Optional<ChildEntity> findByUserIdAndId(Long userId, Long id);

    Optional<ChildEntity> findByUser(UserEntity user);

    List<ChildEntity> findAllByUserId(Long id);


}
