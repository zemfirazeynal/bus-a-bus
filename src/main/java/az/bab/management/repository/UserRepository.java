package az.bab.management.repository;


import az.bab.management.domain.UserEntity;
import az.bab.management.domain.select.UserRolePermissionSelectEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {
    Optional<UserEntity> findByEmail(String name);

    Optional<UserEntity> findByUsername(String name);

    @Query(nativeQuery = true, value = "select\n" +
            "    u.email,\n" +
            "    u.password,\n" +
            "    r.name as role,\n" +
            "    concat(concat(lower(r.name), '::'), lower(p.name)) as permission\n" +
            "from\n" +
            "    user u\n" +
            "    join `bus-a-bus`.user_role_permission urp on u.id = urp.user_id\n" +
            "    join `bus-a-bus`.role_permission rp on urp.role_permission_id = rp.id\n" +
            "    join `bus-a-bus`.role r on rp.role_id = r.id\n" +
            "    join `bus-a-bus`.permission p on p.id = rp.permission_id\n" +
            "where\n" +
            "    u.username = :username")
    List<UserRolePermissionSelectEntity> findByEmailForRoleAndPermission(@Param("username") String username);
}
