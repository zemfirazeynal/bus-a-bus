package az.bab.management.config;

import az.bab.management.domain.UserEntity;
import az.bab.management.domain.UserRolePermissionEntity;
import az.bab.management.domain.select.UserRolePermissionSelectEntity;
import az.bab.management.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<UserEntity> userOptional = userRepository.findByUsername(username);
        List<UserRolePermissionSelectEntity> permissionEntities = userRepository.findByEmailForRoleAndPermission(username);
        if (userOptional.isEmpty()) {
            throw new UsernameNotFoundException("User not found" + username);
        }

        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        List<String> roles = permissionEntities.stream().map(UserRolePermissionSelectEntity::getRole).distinct().toList();
        List<String> permissions = permissionEntities.stream().map(UserRolePermissionSelectEntity::getPermission).toList();

        roles.forEach(role -> authorities.add(new SimpleGrantedAuthority(role)));
        permissions.forEach(permission -> authorities.add(new SimpleGrantedAuthority(permission)));

        if (userOptional.isPresent()) {
            UserEntity userEntity = userOptional.get();
            return User.builder()
                    .username(userEntity.getUsername())
                    .password(userEntity.getPassword())
                    .authorities(authorities)
                    .build();
        } else {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
    }
}