package az.bab.management.config;

import az.bab.management.domain.UserEntity;
import az.bab.management.repository.UserRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserService;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class CustomOAuth2UserService implements OAuth2UserService<OAuth2UserRequest, OAuth2User> {

    @Autowired
    private UserRepository userRepository;

    @Override
    public OAuth2User loadUser(OAuth2UserRequest userRequest) throws OAuth2AuthenticationException {
        OAuth2UserService<OAuth2UserRequest, OAuth2User> delegate = new DefaultOAuth2UserService();
        OAuth2User oAuth2User = delegate.loadUser(userRequest);

        String email = oAuth2User.getAttribute("email");
        String name = oAuth2User.getAttribute("given_name");
        String surname = oAuth2User.getAttribute("family_name");


        Optional<UserEntity> userOptional = userRepository.findByUsername(email.split("@")[0]);
        UserEntity userEntity = new UserEntity();
        if (!userOptional.isPresent()) {
            userEntity.setEmail(email);
            userEntity.setUsername(email.split("@")[0]);
            userEntity.setName(name);
            userEntity.setSurname(surname);
            userEntity.setProvider("GOOGLE");
            userEntity.setPassword(new BCryptPasswordEncoder().encode(
                    RandomStringUtils.random(10, true, true)));

            userRepository.save(userEntity);
        }

        return new CustomOAuth2User(oAuth2User);
    }
}