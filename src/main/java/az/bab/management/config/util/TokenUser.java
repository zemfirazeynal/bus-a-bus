package az.bab.management.config.util;

import az.bab.management.domain.UserEntity;
import az.bab.management.exception.GeneralException;
import az.bab.management.exception.utils.TechExceptionCodes;
import az.bab.management.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

@Component
public class TokenUser {

    @Autowired
    private UserRepository userRepository;

    public UserEntity userDetail() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User userToken = (User) authentication.getPrincipal();
        String username = userToken.getUsername();
        UserEntity user = userRepository.findByUsername(username).orElseThrow(() -> new GeneralException(TechExceptionCodes.USER_NOT_FOUND, HttpStatus.NOT_FOUND));
        return user;
    }

}

