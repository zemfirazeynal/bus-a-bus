package az.bab.management.mapper;

import az.bab.management.model.request.PhoneNumberRequest;
import az.bab.management.domain.PhoneNumberEntity;
import az.bab.management.model.response.PhoneNumberResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;


@Mapper(componentModel = "spring")
public interface PhoneNumberMapper {
    PhoneNumberMapper INSTANCE = Mappers.getMapper(PhoneNumberMapper.class);


    @Mapping(source = "user.id", target = "userId")
    PhoneNumberResponse toDto(PhoneNumberEntity entity);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "updateDate", ignore = true)
    PhoneNumberEntity toDbo(PhoneNumberRequest dto);

    List<PhoneNumberEntity> toDbo(List<PhoneNumberRequest> dto);
}
