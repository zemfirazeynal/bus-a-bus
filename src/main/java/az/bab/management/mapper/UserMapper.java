package az.bab.management.mapper;


import az.bab.management.model.request.UserRequest;
import az.bab.management.domain.PhoneNumberEntity;
import az.bab.management.domain.UserEntity;
import az.bab.management.model.response.PhoneNumberResponse;
import az.bab.management.model.response.UserResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);


    @Mapping(target = "userId", source = "user.id")
    PhoneNumberResponse toDto(PhoneNumberEntity entity);

    List<PhoneNumberResponse> toDtoList(List<PhoneNumberEntity> entity);

    UserResponse toDto(UserEntity entity);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createDate", ignore = true)
    @Mapping(target = "updateDate", ignore = true)
    @Mapping(target = "phoneNumbers", ignore = true)
    UserEntity toDbo(UserRequest dto);

}