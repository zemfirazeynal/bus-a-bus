package az.bab.management.mapper;

import az.bab.management.domain.PermissionEntity;
import az.bab.management.model.request.PermissionRequest;
import az.bab.management.model.response.PermissionResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface PermissionMapper {

    PermissionMapper INSTANCE = Mappers.getMapper(PermissionMapper.class);


    PermissionResponse toDto(PermissionEntity entity);

    @Mapping(target = "id", ignore = true)
    PermissionEntity toDbo(PermissionRequest dto);
}
