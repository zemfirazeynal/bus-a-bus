package az.bab.management.mapper;

import az.bab.management.domain.RolePermissionEntity;
import az.bab.management.model.request.RolePermissionRequest;
import az.bab.management.model.response.RolePermissionResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface RolePermissionMapper {

    RolePermissionMapper INSTANCE = Mappers.getMapper(RolePermissionMapper.class);


    RolePermissionResponse toDto(RolePermissionEntity entity);

    @Mapping(target = "id", ignore = true)
    RolePermissionEntity toDbo(RolePermissionRequest dto);
}
