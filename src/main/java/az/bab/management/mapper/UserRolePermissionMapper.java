package az.bab.management.mapper;

import az.bab.management.domain.UserRolePermissionEntity;
import az.bab.management.model.request.UserRolePermissionRequest;
import az.bab.management.model.response.UserRolePermissionResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface UserRolePermissionMapper {

    UserRolePermissionMapper INSTANCE = Mappers.getMapper(UserRolePermissionMapper.class);


    UserRolePermissionResponse toDto(UserRolePermissionEntity entity);

    @Mapping(target = "id", ignore = true)
    UserRolePermissionRequest toDbo(UserRolePermissionRequest dto);
}
