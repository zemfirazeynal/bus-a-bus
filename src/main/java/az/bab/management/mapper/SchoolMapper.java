package az.bab.management.mapper;

import az.bab.management.model.request.SchoolRequest;
import az.bab.management.model.response.SchoolResponse;

import az.bab.management.domain.SchoolEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;


@Mapper(componentModel = "spring")
public interface SchoolMapper {
    SchoolMapper INSTANCE = Mappers.getMapper(SchoolMapper.class);

    SchoolResponse toDto(SchoolEntity entity);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createDate", ignore = true)
    @Mapping(target = "updateDate", ignore = true)
    SchoolEntity toDbo(SchoolRequest schoolRequest);

}
