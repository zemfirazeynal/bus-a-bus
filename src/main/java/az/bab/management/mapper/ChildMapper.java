package az.bab.management.mapper;

import az.bab.management.domain.ChildEntity;
import az.bab.management.model.request.ChildRequest;
import az.bab.management.model.response.ChildResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;


@Mapper(componentModel = "spring")
public interface ChildMapper {
    ChildMapper INSTANCE = Mappers.getMapper(ChildMapper.class);

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "school.id", target = "schoolId")
    ChildResponse toDto(ChildEntity childEntity);

    @Mapping(target = "id", ignore = true)
    @Mapping(source = "schoolId", target = "school.id")
    @Mapping(target = "createDate", ignore = true)
    @Mapping(target = "updateDate", ignore = true)
    ChildEntity toDbo(ChildRequest childRequest);

}
