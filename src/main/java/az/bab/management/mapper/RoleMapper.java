package az.bab.management.mapper;

import az.bab.management.domain.RoleEntity;
import az.bab.management.model.request.RoleRequest;

import az.bab.management.model.response.RoleResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface RoleMapper {

    RoleMapper INSTANCE = Mappers.getMapper(RoleMapper.class);


    RoleResponse toDto(RoleEntity entity);

    @Mapping(target = "id", ignore = true)
    RoleEntity toDbo(RoleRequest dto);

}
