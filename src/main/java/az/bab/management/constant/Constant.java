package az.bab.management.constant;

public final class Constant {

    private Constant() {
        throw new IllegalStateException("you don't have access for creating object.");
    }
}
