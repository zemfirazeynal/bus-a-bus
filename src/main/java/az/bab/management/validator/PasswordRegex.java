package az.bab.management.validator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = PasswordValidator.class)
public @interface PasswordRegex {
    /**
     * error message.
     */
    String message() default "Password is not suitable for the process";

    /**
     * represents group of constraints.
     */
    Class<?>[] groups() default {};

    /**
     * represents additional information about annotation.
     */

    Class<? extends Payload>[] payload() default {};
}
