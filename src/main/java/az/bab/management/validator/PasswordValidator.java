package az.bab.management.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.util.StringUtils;

import java.util.regex.Pattern;


public class PasswordValidator implements ConstraintValidator<PasswordRegex, String> {


    public static final Pattern PASSWORD = Pattern.compile("^[A-Za-z0-9+_.-].{9,21}$");



    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return StringUtils.hasText(value) && PASSWORD.matcher(value).matches();

    }

}
