package az.bab.management.exception;

import az.bab.management.exception.utils.TechExceptionCodes;
import org.springframework.http.HttpStatus;

public class TechException extends GeneralException {
    private static final long serialVersionUID = 58434214877811L;

    public TechException(TechExceptionCodes techExceptionCodes) {
        super(techExceptionCodes, HttpStatus.BAD_REQUEST);
    }

    public TechException(TechExceptionCodes techExceptionCodes, HttpStatus httpStatus) {
        super(techExceptionCodes, httpStatus);
    }

    public TechException(String message, TechExceptionCodes techExceptionCodes, HttpStatus httpStatus) {
        super(message, techExceptionCodes, httpStatus);
    }

    public TechException(Throwable cause, TechExceptionCodes techExceptionCodes, HttpStatus httpStatus) {
        super(cause, techExceptionCodes, httpStatus);
    }
}
