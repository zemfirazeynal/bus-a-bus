package az.bab.management.exception.utils;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ValidationError {
    private String rejectedValue;
    private String rejectedMessage;
}
