package az.bab.management.exception.utils;

public enum BusinessExceptionCodes implements ExceptionKeyAndMessage {

    COMMON_SUCCESS("0", "Successfully process");

    private final String code;
    private final String message;

    BusinessExceptionCodes(String code, String message) {
        this.code = code;
        this.message = message;
    }


    @Override
    public String getExceptionCode() {
        return code;
    }

    @Override
    public String getExceptionMessage() {
        return message;
    }
}
