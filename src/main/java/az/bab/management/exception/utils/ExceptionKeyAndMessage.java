package az.bab.management.exception.utils;

public interface ExceptionKeyAndMessage {


    String getExceptionCode();

    String getExceptionMessage();

}
