package az.bab.management.exception;

import az.bab.management.exception.utils.ExceptionKeyAndMessage;
import org.springframework.http.HttpStatus;

public class GeneralException extends RuntimeException {

    private static final long serialVersionUID = 853354321283511L;

    private final String code;
    private final HttpStatus httpStatus;

    private String key;

    public GeneralException(Enum<? extends ExceptionKeyAndMessage> keyAndMessage,
                            HttpStatus httpStatus) {
        super(((ExceptionKeyAndMessage) keyAndMessage).getExceptionMessage());
        ExceptionKeyAndMessage key = (ExceptionKeyAndMessage) keyAndMessage;
        this.code = key.getExceptionCode();
        this.httpStatus = httpStatus;
    }

    public GeneralException(String message,
                            Enum<? extends ExceptionKeyAndMessage> keyAndMessage,
                            HttpStatus httpStatus) {
        super(((ExceptionKeyAndMessage) keyAndMessage).getExceptionMessage() + " " + message);
        ExceptionKeyAndMessage key = (ExceptionKeyAndMessage) keyAndMessage;
        this.code = key.getExceptionCode();
        this.httpStatus = httpStatus;
    }

    public GeneralException(Throwable cause,
                            Enum<? extends ExceptionKeyAndMessage> keyAndMessage,
                            HttpStatus httpStatus) {
        super(cause);
        ExceptionKeyAndMessage key = (ExceptionKeyAndMessage) keyAndMessage;
        this.code = key.getExceptionCode();
        this.httpStatus = httpStatus;
    }

    public GeneralException(String message,
                            String code,
                            HttpStatus httpStatus) {
        super(message);
        this.code = code;
        this.httpStatus = httpStatus;
    }

    public String getCode() {
        return code;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getKey() {
        return key;
    }
}
