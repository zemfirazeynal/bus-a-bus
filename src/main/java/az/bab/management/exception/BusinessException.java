package az.bab.management.exception;

import az.bab.management.exception.utils.BusinessExceptionCodes;
import org.springframework.http.HttpStatus;

public class BusinessException extends GeneralException {


    private static final long serialVersionUID = 58434214877811L;

    public BusinessException(BusinessExceptionCodes buisnessExceptionCodes) {
        super(buisnessExceptionCodes, HttpStatus.NOT_FOUND);
    }

    public BusinessException(BusinessExceptionCodes buisnessExceptionCodes, HttpStatus httpStatus) {
        super(buisnessExceptionCodes, httpStatus);
    }

    public BusinessException(String message, BusinessExceptionCodes buisnessExceptionCodes, HttpStatus httpStatus) {
        super(message, buisnessExceptionCodes, httpStatus);
    }

    public BusinessException(Throwable cause, BusinessExceptionCodes buisnessExceptionCodes, HttpStatus httpStatus) {
        super(cause, buisnessExceptionCodes, httpStatus);
    }
}
