package az.bab.management.exception.handler;


import az.bab.management.exception.GeneralException;
import az.bab.management.exception.utils.ExceptionKeyAndMessage;
import az.bab.management.exception.utils.HttpResponseConstants;
import az.bab.management.exception.utils.TechExceptionCodes;

import az.bab.management.exception.utils.ValidationError;
import az.bab.management.model.response.CommonResponse;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.net.ConnectException;
import java.net.SocketException;
import java.nio.file.AccessDeniedException;
import java.security.KeyStoreException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    private static final String PROJECT_KEY_NAME = "BUS_A_BUS: ";


    @ExceptionHandler(GeneralException.class)
    public ResponseEntity<Object> handle(GeneralException ex,
                                         WebRequest request) {
        log.error("GeneralException: {}", ex.getMessage());
        return ofType(request,
                ex.getHttpStatus(),
                ex.getMessage(),
                ex.getCode(),
                Collections.emptyList());
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<Object> handle(AccessDeniedException ex,
                                         WebRequest request) {
        log.error("Access denied: {}", ex.getMessage());
        return ofType(ex, request, TechExceptionCodes.ACCESS_DENIED, HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public ResponseEntity<Object> handle(MaxUploadSizeExceededException ex,
                                         WebRequest request) {
        log.error("Max Upload Size Exceeded Exception: {}", ex.getMessage());
        return ofType(ex,
                request,
                TechExceptionCodes.MAX_UPLOAD_SIZE_EXCEEDED,
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(ConnectException.class)
    public ResponseEntity<Object> handle(ConnectException ex,
                                         WebRequest request) {
        log.error("Connect Exception: {}", ex.getMessage());
        return ofType(ex,
                request,
                TechExceptionCodes.CONNECT_EXCEPTION,
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<Object> handle(MethodArgumentTypeMismatchException ex,
                                         WebRequest request) {
        log.error("MethodArgumentTypeMismatchException: {}", ex.getMessage());
        return ofType(ex,
                request,
                TechExceptionCodes.METHOD_ARGUMENT_TYPE_MISMATCH,
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<Object> handle(NoSuchElementException ex,
                                         WebRequest request) {
        log.error("NoSuchElementException: {}", ex.getMessage());
        return ofType(ex,
                request,
                TechExceptionCodes.NO_SUCH_ELEMENT,
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(KeyStoreException.class)
    public ResponseEntity<Object> handle(KeyStoreException ex,
                                         WebRequest request) {
        log.error("KeyStoreException: {}", ex.getMessage());
        return ofType(ex,
                request,
                TechExceptionCodes.KEY_STORE_EXCEPTION,
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(SocketException.class)
    public ResponseEntity<Object> handle(SocketException ex,
                                         WebRequest request) {
        log.error("SocketException: {}", ex.getMessage());
        return ofType(ex,
                request,
                TechExceptionCodes.SOCKET_EXCEPTION,
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(DateTimeParseException.class)
    public ResponseEntity<Object> handle(DateTimeParseException ex,
                                         WebRequest request) {
        log.error("DateTimeParseException: {}", ex.getMessage());
        return ofType(ex,
                request,
                TechExceptionCodes.DATE_TIME_PARSE,
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(InvalidFormatException.class)
    public ResponseEntity<Object> handle(InvalidFormatException ex,
                                         WebRequest request) {
        log.error("InvalidFormatException: {}", ex.getMessage());
        return ofType(ex,
                request,
                TechExceptionCodes.INVALID_FORMAT,
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(NoSuchBeanDefinitionException.class)
    public ResponseEntity<Object> handle(NoSuchBeanDefinitionException ex,
                                         WebRequest request) {
        log.error("NoSuchBeanDefinitionException: {}", ex.getMessage());
        return ofType(ex,
                request,
                TechExceptionCodes.NO_SUCH_BEAN_DEFINITION,
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<Object> handle(NullPointerException ex,
                                         WebRequest request) {
        log.error("NullPointerException: {}", ex.getMessage());
        return ofType(ex,
                request,
                TechExceptionCodes.NULL_POINTER,
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Object> handle(IllegalArgumentException ex,
                                         WebRequest request) {
        log.error("IllegalArgumentException: {}", ex.getMessage());
        return ofType(ex,
                request,
                TechExceptionCodes.ILLEGAL_ARGUMENT,
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(IllegalAccessException.class)
    public ResponseEntity<Object> handle(IllegalAccessException ex,
                                         WebRequest request) {
        log.error("IllegalAccessException: {}", ex.getMessage());
        return ofType(ex,
                request,
                TechExceptionCodes.ILLEGAL_ARGUMENT,
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(ConversionFailedException.class)
    public ResponseEntity<Object> handle(ConversionFailedException ex,
                                         WebRequest request) {
        log.error("ConversionFailedException: {}", ex.getMessage());
        return ofType(ex,
                request,
                TechExceptionCodes.CONVERSION_EXCEPTION,
                HttpStatus.BAD_REQUEST
        );
    }



    @ExceptionHandler(ArithmeticException.class)
    public ResponseEntity<Object> handle(ArithmeticException ex,
                                         WebRequest request) {
        log.error("ArithmeticException: {}", ex.getMessage());
        return ofType(ex,
                request,
                TechExceptionCodes.ARITHMETIC,
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(ClassCastException.class)
    public ResponseEntity<Object> handle(ClassCastException ex,
                                         WebRequest request) {
        log.error("ClassCastException: {}", ex.getMessage());
        return ofType(ex,
                request,
                TechExceptionCodes.CLASS_CAST_EXCEPTION,
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<Object> handle(RuntimeException ex,
                                         WebRequest request) {
        log.error("RuntimeException: {}", ex.getMessage());
        return ofType(
                request,
                HttpStatus.BAD_REQUEST,
                TechExceptionCodes.RUNTIME_EXCEPTION.getExceptionMessage(),
                TechExceptionCodes.RUNTIME_EXCEPTION.getExceptionCode(),
                Collections.emptyList()
        );
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> handle(ConstraintViolationException ex,
                                         WebRequest request) {
        Set<ConstraintViolation<?>> constraintViolations = ex.getConstraintViolations();
        List<ValidationError> validationErrors = constraintViolations.stream()
                .map(p -> ValidationError.builder()
                        .rejectedValue(Objects.requireNonNull(p.getInvalidValue()).toString())
                        .rejectedMessage(p.getMessage()).build()).collect(Collectors.toList());
        log.error("javax.validation.ConstraintViolationException: {}", ex.getMessage());
        return ofType(
                request,
                HttpStatus.BAD_REQUEST,
                TechExceptionCodes.CONSTRAINT_VIOLATION.getExceptionMessage(),
                TechExceptionCodes.CONSTRAINT_VIOLATION.getExceptionCode(),
                validationErrors);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handle(MethodArgumentNotValidException ex,
                                         WebRequest request) {
        log.error("MethodArgumentNotValidException: {}", ex.getMessage());
        List<ValidationError> validationErrors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(e -> ValidationError.builder()
                        .rejectedValue(Objects.requireNonNullElse(e.getRejectedValue(), "null").toString())
                        .rejectedMessage(e.getDefaultMessage()).build())
                .collect(Collectors.toList());
        return ofType(request,
                HttpStatus.BAD_REQUEST,
                TechExceptionCodes.METHOD_ARGUMENT_NOT_VALID.getExceptionMessage(),
                TechExceptionCodes.METHOD_ARGUMENT_NOT_VALID.getExceptionCode(),
                validationErrors);
    }


    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<Object> handle(HttpRequestMethodNotSupportedException ex,
                                         WebRequest request) {
        log.error("HttpRequestMethodNotSupportedException: {}", ex.getMessage());
        return ofType(ex, request,
                TechExceptionCodes.REQUEST_METHOD_NOT_SUPPORTED, HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public ResponseEntity<Object> handle(HttpMediaTypeNotSupportedException ex,
                                         WebRequest request) {
        log.error("HttpMediaTypeNotSupportedException: {}", ex.getMessage());
        return ofType(ex, request,
                TechExceptionCodes.MEDIA_TYPE_NOT_SUPPORTED, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
    public ResponseEntity<Object> handle(HttpMediaTypeNotAcceptableException ex,
                                         WebRequest request) {
        log.error("HttpMediaTypeNotAcceptableException: {}", ex.getMessage());
        return ofType(ex, request,
                TechExceptionCodes.MEDIA_TYPE_NOT_ACCEPTABLE, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MissingPathVariableException.class)
    public ResponseEntity<Object> handle(MissingPathVariableException ex,
                                         WebRequest request) {
        log.error("MissingPathVariableException: {}", ex.getMessage());
        return ofType(ex, request,
                TechExceptionCodes.MISSING_PATH_VARIABLE, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<Object> handle(MissingServletRequestParameterException ex,
                                         WebRequest request) {
        log.error("MissingServletRequestParameterException: {}", ex.getMessage());
        return ofType(ex, request,
                TechExceptionCodes.MISSING_SERVLET_REQUEST_PARAMETER, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ServletRequestBindingException.class)
    public ResponseEntity<Object> handle(ServletRequestBindingException ex,
                                         WebRequest request) {
        log.error("ServletRequestBindingException: {}", ex.getMessage());
        return ofType(ex, request,
                TechExceptionCodes.SERVLET_REQUEST_BINDING, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConversionNotSupportedException.class)
    public ResponseEntity<Object> handle(ConversionNotSupportedException ex,
                                         WebRequest request) {
        log.error("ConversionNotSupportedException: {}", ex.getMessage());
        return ofType(ex, request,
                TechExceptionCodes.CONVERSION_NOT_SUPPORTED, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(TypeMismatchException.class)
    public ResponseEntity<Object> handle(TypeMismatchException ex,
                                         WebRequest request) {
        log.error("TypeMismatchException: {}", ex.getMessage());
        return ofType(ex, request,
                TechExceptionCodes.TYPE_MISMATCH, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<Object> handle(HttpMessageNotReadableException ex,
                                         WebRequest request) {
        log.error("HttpMessageNotReadableException: {}", ex.getMessage());
        return ofType(ex, request,
                TechExceptionCodes.MESSAGE_NOT_READABLE, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(HttpMessageNotWritableException.class)
    public ResponseEntity<Object> handle(HttpMessageNotWritableException ex,
                                         WebRequest request) {
        log.error("HttpMessageNotWritableException: {}", ex.getMessage());
        return ofType(ex, request,
                TechExceptionCodes.MESSAGE_NOT_WRITABLE, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MissingServletRequestPartException.class)
    public ResponseEntity<Object> handle(MissingServletRequestPartException ex,
                                         WebRequest request) {
        log.error("MissingServletRequestPartException: {}", ex.getMessage());
        return ofType(ex, request,
                TechExceptionCodes.MISSING_SERVLET_REQUEST_PART, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(BindException.class)
    public ResponseEntity<Object> handle(BindException ex,
                                         WebRequest request) {
        log.error("BindException: {}", ex.getMessage());
        return ofType(ex, request,
                TechExceptionCodes.BIND_EXCEPTION, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public ResponseEntity<Object> handle(NoHandlerFoundException ex,
                                         WebRequest request) {
        log.error("NoHandlerFoundException: {}", ex.getMessage());
        return ofType(ex, request,
                TechExceptionCodes.NO_HANDLER_FOUND, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AsyncRequestTimeoutException.class)
    public ResponseEntity<Object> handle(AsyncRequestTimeoutException ex,
                                         WebRequest request) {
        log.error("AsyncRequestTimeoutException: {}", ex.getMessage());
        return ofType(ex, request,
                TechExceptionCodes.ASYNC_REQUEST_TIMEOUT, HttpStatus.BAD_REQUEST);
    }


    private ResponseEntity<Object> ofType(Exception ex,
                                          WebRequest request,
                                          Enum<? extends ExceptionKeyAndMessage> globalExceptionCodes,
                                          HttpStatus httpStatus) {
        return ofType(request,
                httpStatus,
                ((ExceptionKeyAndMessage) globalExceptionCodes).getExceptionMessage() + "(" + ex.getMessage() + ")",
                ((ExceptionKeyAndMessage) globalExceptionCodes).getExceptionCode(),
                Collections.emptyList());
    }


    private ResponseEntity<Object> ofType(WebRequest request,
                                          HttpStatus status,
                                          String message,
                                          String code,
                                          List<?> validationErrors) {
        Map<String, Object> attributes = new HashMap<>();
        attributes.put(HttpResponseConstants.CODE, code);
        attributes.put(HttpResponseConstants.MESSAGE, message);
        attributes.put(HttpResponseConstants.TECH_MESSAGE, message);
        attributes.put(HttpResponseConstants.STATUS, status.value());
        attributes.put(HttpResponseConstants.ERROR, status.getReasonPhrase());
        if (!validationErrors.isEmpty()) {
            attributes.put(HttpResponseConstants.VALIDATION_ERRORS, validationErrors);
        }
        attributes.put(HttpResponseConstants.KEY, PROJECT_KEY_NAME + code);
        attributes.put(HttpResponseConstants.TIMESTAMP,
                LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        attributes.put(HttpResponseConstants.PATH, ((ServletWebRequest) request).getRequest().getRequestURI());
        return new ResponseEntity<>(attributes, status);
    }
}
