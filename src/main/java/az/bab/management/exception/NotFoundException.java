package az.bab.management.exception;

import org.springframework.http.HttpStatus;

public class NotFoundException extends GeneralException {
    public NotFoundException(String message, String code, HttpStatus httpStatus) {
        super(message, code, httpStatus);
    }
}
