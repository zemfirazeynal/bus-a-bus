package az.bab.management.domain;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;


@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "permission")
public class PermissionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;


    @OneToMany(mappedBy = "permission")
    private List<RolePermissionEntity> rolePermissionEntities;

    private LocalDateTime createDate;
    private LocalDateTime updateDate;

    @PrePersist
    public void insert() {
        createDate = LocalDateTime.now();
        updateDate = LocalDateTime.now();

    }


    @PreUpdate
    public void update() {
        updateDate = LocalDateTime.now();
    }
}
