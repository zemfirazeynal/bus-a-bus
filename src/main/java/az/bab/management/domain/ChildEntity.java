package az.bab.management.domain;


import az.bab.management.controller.ChildController;
import az.bab.management.service.impl.ChildServiceImpl;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "child")
public class ChildEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String surname;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private UserEntity user;

    @ManyToOne
    @JoinColumn(name = "school_id", nullable = false)
    private SchoolEntity school;

    private LocalDateTime createDate;
    private LocalDateTime updateDate;


    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public void setSchool(SchoolEntity school) {
        this.school = school;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public void setUpdateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
    }

    @PrePersist
    public void insert() {
        createDate = LocalDateTime.now();
        updateDate = LocalDateTime.now();

    }

    @PreUpdate
    public void update() {
        updateDate = LocalDateTime.now();
    }
}
