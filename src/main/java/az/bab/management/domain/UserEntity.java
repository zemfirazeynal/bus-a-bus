package az.bab.management.domain;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.Builder;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "user")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String surname;
    private String email;
    private String username;
    private String password;
    private String pickUpAddress;
    private String dropOffAddress;
    private LocalDateTime createDate;
    private LocalDateTime updateDate;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<ChildEntity> children;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<PhoneNumberEntity> phoneNumbers;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<UserRolePermissionEntity> userRolePermissionEntities;

    private String provider; // For tracking the provider (e.g., local, google)

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPickUpAddress(String pickUpAddress) {
        this.pickUpAddress = pickUpAddress;
    }

    public void setDropOffAddress(String dropOffAddress) {
        this.dropOffAddress = dropOffAddress;
    }


    public void setPhoneNumbers(List<PhoneNumberEntity> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
        this.phoneNumbers.forEach(phoneNumber -> phoneNumber.setUser(this));
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    @PrePersist
    public void insert() {
        createDate = LocalDateTime.now();
        updateDate = LocalDateTime.now();
        if (username == null) {
            setUsername(email.split("@")[0]);
        }
        if (provider == null) {
            provider = "BASE";
        }
    }


    @PreUpdate
    public void update() {
        updateDate = LocalDateTime.now();
    }

    //@JsonIgnore
}