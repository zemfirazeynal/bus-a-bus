package az.bab.management.domain;


import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "user_role_permission")
public class UserRolePermissionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @ManyToOne
    @JoinColumn(name = "role_permission_id")
    private RolePermissionEntity rolePermission;

    private LocalDateTime createDate;
    private LocalDateTime updateDate;

    @PrePersist
    public void insert() {
        createDate = LocalDateTime.now();
        updateDate = LocalDateTime.now();

    }


    @PreUpdate
    public void update() {
        updateDate = LocalDateTime.now();
    }
}
