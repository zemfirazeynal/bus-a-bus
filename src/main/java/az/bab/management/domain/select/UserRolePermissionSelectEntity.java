package az.bab.management.domain.select;


public interface UserRolePermissionSelectEntity {

    String getEmail();

    String getPassword();

    String getRole();

    String getPermission();
}
