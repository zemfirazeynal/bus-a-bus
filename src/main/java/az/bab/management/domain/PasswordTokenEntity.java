package az.bab.management.domain;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "password_token")
public class PasswordTokenEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long userId;
    private String token;
    private LocalDateTime createDate;
    private LocalDateTime updateDate;

    @PrePersist
    public void insert() {
        createDate = LocalDateTime.now();
        updateDate = LocalDateTime.now();

    }

    @PreUpdate
    public void update() {
        updateDate = LocalDateTime.now();
    }

}
