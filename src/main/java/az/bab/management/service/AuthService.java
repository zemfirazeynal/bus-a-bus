package az.bab.management.service;

import az.bab.management.model.request.AuthenticationRequest;
import az.bab.management.model.request.EmailRequest;
import az.bab.management.model.request.PasswordRequest;
import az.bab.management.model.request.UserRequest;
import az.bab.management.model.response.AuthenticationResponse;
import az.bab.management.model.response.UserResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public interface AuthService {

    ResponseEntity<?> registerUser(UserRequest userRequest);

    UserResponse updateGoogleUser(UserRequest userRequest);

    AuthenticationResponse authenticateUser(AuthenticationRequest authRequest);

    ResponseEntity<?> forgotPassword(EmailRequest emailRequest);

    ResponseEntity<?> resetPassword(String password, String token);
}