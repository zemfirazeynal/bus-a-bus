package az.bab.management.service;


import az.bab.management.model.request.PhoneNumberRequest;
import az.bab.management.model.response.PhoneNumberResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface PhoneNumberService {
    PhoneNumberResponse createPhoneNumber(PhoneNumberRequest phoneNumberRequest);

    PhoneNumberResponse updatePhoneNumber(Long phoneNumberId, PhoneNumberRequest phoneNumberRequest);

    PhoneNumberResponse updatePhoneNumber(Long userId,Long phoneNumberId, PhoneNumberRequest phoneNumberRequest);

    void deletePhoneNumber(Long phoneNumberId);

    void deletePhoneNumber(Long userId, Long phoneNumberId);

}
