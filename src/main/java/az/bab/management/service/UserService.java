package az.bab.management.service;


import az.bab.management.model.request.PasswordRequest;
import az.bab.management.model.request.UserRequest;
import az.bab.management.model.response.UserResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Service
public interface UserService {

    List<UserResponse> getAllUsers();

    UserResponse getUserInfo();

    UserResponse getUserById(Long id);


    void deleteUser(Long id,UserRequest userRequest);

    UserResponse updateUserFields(UserRequest userRequest);

    UserResponse updateUserPassword(PasswordRequest passwordRequest);
}