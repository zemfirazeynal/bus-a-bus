package az.bab.management.service;

import jakarta.mail.internet.MimeMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.io.File;

@Slf4j
@Service
public class SendMailServices {

    @Autowired
    JavaMailSender javaMailSender;

    public String sendPasswordResetEmail(String email, String token) {
        String url = "http://localhost:8099/reset-password/" + token;
        String msj = "Click the following link to reset your password: " + url;

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("zemfirazeynal@gmail.com");
        message.setTo(email);
        message.setSubject("BUSABUS PASSWORD UPDATE");
        message.setText(msj);

        javaMailSender.send(message);
        return "Successfully!";
    }
}