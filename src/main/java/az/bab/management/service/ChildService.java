package az.bab.management.service;


import az.bab.management.model.request.ChildRequest;
import az.bab.management.model.response.ChildResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import java.util.List;

@Service
public interface ChildService {

    List<ChildResponse> getAllUserChildren();

    List<ChildResponse> getAllUserChildren(Long userId);

    List<ChildResponse> getAllChildren();

    ChildResponse createChild(ChildRequest childRequest);

    ChildResponse updateChildFields(Long id, ChildRequest childRequest);

    ChildResponse updateChildFields(Long userId,Long id, ChildRequest childRequest);

    void deleteChild(Long id);

    void deleteChild(Long userId, Long id);

}
