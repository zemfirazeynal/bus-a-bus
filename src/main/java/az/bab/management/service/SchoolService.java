package az.bab.management.service;

import az.bab.management.model.request.SchoolRequest;
import az.bab.management.model.response.SchoolResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public interface SchoolService {
    SchoolResponse createSchool(SchoolRequest schoolRequest);

    List<SchoolResponse> getAllSchools();

    SchoolResponse updateSchoolFields(Long id, SchoolRequest schoolRequest);

    void deleteSchool(Long id, SchoolRequest schoolRequest);
}
