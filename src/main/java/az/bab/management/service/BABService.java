package az.bab.management.service;

import az.bab.management.domain.UserEntity;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Service
public interface BABService {
    Optional<UserEntity> findByUsername(String username);

    Optional<UserEntity> findByEmail(String email);

    List<UserEntity> findAll();

    String login();

    Principal dashboard(Principal principal);

    List<UserEntity> allUser();

}
