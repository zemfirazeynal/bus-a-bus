package az.bab.management.service.impl;

import az.bab.management.exception.GeneralException;
import az.bab.management.exception.utils.TechExceptionCodes;
import az.bab.management.model.request.SchoolRequest;
import az.bab.management.model.response.SchoolResponse;
import az.bab.management.domain.SchoolEntity;
import az.bab.management.mapper.SchoolMapper;
import az.bab.management.repository.SchoolRepository;
import az.bab.management.service.SchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SchoolServiceImpl implements SchoolService {


    @Autowired
    private SchoolRepository schoolRepository;

    @Autowired
    private SchoolMapper schoolMapper;

    @Override
    public SchoolResponse createSchool(SchoolRequest schoolRequest) {
        SchoolEntity schoolEntity = schoolMapper.toDbo(schoolRequest);
        SchoolEntity savedSchool = schoolRepository.save(schoolEntity);
        SchoolResponse schoolResponse = schoolMapper.toDto(savedSchool);

        return schoolResponse;

    }

    @Override
    public List<SchoolResponse> getAllSchools() {
        return schoolRepository.findAll()
                .stream()
                .map(schoolMapper::toDto)
                .toList();
    }

    @Override
    public SchoolResponse updateSchoolFields(Long id, SchoolRequest schoolRequest) {
        SchoolEntity schoolEntity = schoolRepository.findById(id).orElseThrow(() -> new GeneralException(TechExceptionCodes.SCHOOL_NOT_FOUND, HttpStatus.NOT_FOUND));

        Optional.ofNullable(schoolRequest.getName()).ifPresent(schoolEntity::setName);
        Optional.ofNullable(schoolRequest.getNumber()).ifPresent(schoolEntity::setNumber);
        Optional.ofNullable(schoolRequest.getAddress()).ifPresent(schoolEntity::setAddress);


        SchoolEntity updatedschoolEntity = schoolRepository.save(schoolEntity);
        SchoolResponse schoolResponse = schoolMapper.toDto(updatedschoolEntity);

        return schoolResponse;
    }

    @Override
    public void deleteSchool(Long id, SchoolRequest schoolRequest) {
        Optional<SchoolEntity> schoolEntity = Optional.ofNullable(schoolRepository.findById(id).orElseThrow(() -> new GeneralException(TechExceptionCodes.SCHOOL_NOT_FOUND, HttpStatus.NOT_FOUND)));
        if (schoolEntity.isPresent()) {
            schoolRepository.delete(schoolEntity.get());
        }
    }
}
