package az.bab.management.service.impl;

import az.bab.management.config.util.JwtUtil;
import az.bab.management.config.util.TokenUser;
import az.bab.management.domain.PasswordTokenEntity;
import az.bab.management.exception.GeneralException;
import az.bab.management.exception.utils.TechExceptionCodes;
import az.bab.management.model.request.AuthenticationRequest;
import az.bab.management.model.request.EmailRequest;
import az.bab.management.model.request.UserRequest;
import az.bab.management.domain.PhoneNumberEntity;
import az.bab.management.domain.UserEntity;
import az.bab.management.mapper.PhoneNumberMapper;
import az.bab.management.mapper.UserMapper;
import az.bab.management.model.response.AuthenticationResponse;
import az.bab.management.model.response.UserResponse;
import az.bab.management.repository.AuthRepository;
import az.bab.management.repository.PasswordTokenRepository;
import az.bab.management.repository.UserRepository;
import az.bab.management.service.AuthService;
import az.bab.management.service.SendMailServices;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class AuthServiceImpl implements AuthService {

    @Autowired
    private AuthRepository authRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private PhoneNumberMapper phoneNumberMapper;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private TokenUser tokenUser;

    @Autowired
    private SendMailServices sendMailServices;

    @Autowired
    private PasswordTokenRepository passwordTokenRepository;

    @Override
    public ResponseEntity<?> registerUser(UserRequest userRequest) {
        String email = userRequest.getEmail();
        Optional<UserEntity> optionalUser = authRepository.findByEmail(email);
        if (optionalUser.isPresent()) {
            throw new GeneralException(TechExceptionCodes.USER_ALREADY_EXIST, HttpStatus.CONFLICT);
        }
        List<PhoneNumberEntity> phoneNumberEntities = phoneNumberMapper.toDbo(userRequest.getPhoneNumbers());
        userRequest.setPassword(new BCryptPasswordEncoder().encode(userRequest.getPassword()));
        UserEntity userEntity = userMapper.toDbo(userRequest);
        userEntity.setPhoneNumbers(phoneNumberEntities);

        authRepository.save(userEntity);
        return ResponseEntity.ok("User registered successfully");
    }

    @Override
    @Transactional
    public UserResponse updateGoogleUser(UserRequest userRequest) {
        UserEntity optionalUser = userRepository.findById(tokenUser.userDetail().getId()).orElseThrow(() -> new GeneralException(TechExceptionCodes.USER_NOT_FOUND, HttpStatus.NOT_FOUND));

        optionalUser.setPickUpAddress(userRequest.getPickUpAddress());
        optionalUser.setDropOffAddress(userRequest.getDropOffAddress());

        List<PhoneNumberEntity> updatedPhoneNumbers = phoneNumberMapper.toDbo(userRequest.getPhoneNumbers());
        optionalUser.setPhoneNumbers(updatedPhoneNumbers);


        UserEntity userEntity = userRepository.save(optionalUser);
        userEntity.setPhoneNumbers(updatedPhoneNumbers);
        UserResponse userResponse = userMapper.toDto(userEntity);

        return userResponse;

    }

    @Override
    public AuthenticationResponse authenticateUser(AuthenticationRequest authRequest) {
        String email = authRequest.getEmail();
        String password = authRequest.getPassword();
        String username = email.split("@")[0]; // Extract username from email

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (Exception ex) {
            throw new GeneralException(TechExceptionCodes.INVALID_CREDENTIALS, HttpStatus.BAD_REQUEST);
        }

        final UserDetails userDetails = userDetailsService.loadUserByUsername(authRequest.getEmail().split("@")[0]);
        final String jwt = jwtUtil.generateToken(userDetails);
        AuthenticationResponse authenticationResponse = new AuthenticationResponse(jwt);
        return authenticationResponse;
    }

    @Override
    public ResponseEntity<?> forgotPassword(EmailRequest emailRequest) {

        UserEntity user = authRepository.findByEmail(emailRequest.getEmail()).orElseThrow();
        String token = UUID.randomUUID().toString();

        PasswordTokenEntity passwordTokenEntity = new PasswordTokenEntity();
        passwordTokenEntity.setToken(token);
        passwordTokenEntity.setUserId(user.getId());
        passwordTokenRepository.save(passwordTokenEntity);

        sendMailServices.sendPasswordResetEmail(user.getEmail(), token);
        return ResponseEntity.ok("Password reset email sent");
    }

    @Override
    public ResponseEntity<?> resetPassword(String password, String token) {
        PasswordTokenEntity byToken = passwordTokenRepository.findByToken(token).orElseThrow();
        UserEntity userEntity = authRepository.findById(byToken.getUserId()).orElseThrow(() -> new GeneralException(TechExceptionCodes.USER_NOT_FOUND, HttpStatus.NOT_FOUND));
        userEntity.setPassword(new BCryptPasswordEncoder().encode(password));
        authRepository.save(userEntity);
        System.out.println(password + "   " + token);
        return ResponseEntity.ok().build();
    }
}



