package az.bab.management.service.impl;

import az.bab.management.domain.UserEntity;
import az.bab.management.exception.GeneralException;
import az.bab.management.exception.utils.TechExceptionCodes;
import az.bab.management.repository.UserRepository;
import az.bab.management.service.BABService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.net.BindException;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Service
public class BABServiceImpl implements BABService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Optional<UserEntity> findByUsername(String username) {
        return Optional.empty();
    }

    @Override
    public Optional<UserEntity> findByEmail(String email) {
        return Optional.empty();
    }

    @Override
    public List<UserEntity> findAll() {
        return List.of();
    }

    @Override
    public String login() {
        UserEntity user = userRepository.findByUsername("admin").get();
        if (true) {
            System.out.println("burdayam");
        }
        return "";
    }

    @Override
    public Principal dashboard(Principal principal) {
        return null;
    }

    @Override
    public List<UserEntity> allUser() {
        return List.of();
    }
}
