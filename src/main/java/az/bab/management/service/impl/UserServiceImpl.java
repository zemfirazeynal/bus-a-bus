package az.bab.management.service.impl;


import az.bab.management.config.util.TokenUser;
import az.bab.management.exception.GeneralException;
import az.bab.management.exception.utils.TechExceptionCodes;
import az.bab.management.model.request.PasswordRequest;
import az.bab.management.model.request.UserRequest;
import az.bab.management.domain.UserEntity;
import az.bab.management.mapper.PhoneNumberMapper;
import az.bab.management.mapper.UserMapper;
import az.bab.management.model.response.UserResponse;
import az.bab.management.repository.UserRepository;
import az.bab.management.service.UserService;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private PhoneNumberMapper phoneNumberMapper;

    @Autowired
    private TokenUser tokenUser;

    @Override
    public List<UserResponse> getAllUsers() {
        return userRepository.findAll()
                .stream()
                .map(userMapper::toDto)
                .toList();
    }


    @Override
    public UserResponse getUserInfo() {
        UserEntity userEntity = userRepository.findById(tokenUser.userDetail().getId())
                .orElseThrow(() -> new GeneralException(TechExceptionCodes.USER_NOT_FOUND, HttpStatus.NOT_FOUND));
        return userMapper.toDto(userEntity);
    }

    @Override
    public UserResponse getUserById(Long id) {
        UserEntity userEntity = userRepository.findById(id)
                .orElseThrow(() -> new GeneralException(TechExceptionCodes.USER_NOT_FOUND, HttpStatus.NOT_FOUND));

        return userMapper.toDto(userEntity);
    }

    @Override
    public void deleteUser(Long id,UserRequest userRequest) {
        UserEntity userEntity = userRepository.findById(id).orElseThrow(() -> new GeneralException(TechExceptionCodes.USER_NOT_FOUND, HttpStatus.NOT_FOUND));
        userRepository.delete(userEntity);
    }


    @Override
    public UserResponse updateUserFields(UserRequest userRequest) {
        UserEntity userEntity = userRepository.findById(tokenUser.userDetail().getId()).orElseThrow(() -> new GeneralException(TechExceptionCodes.USER_NOT_FOUND, HttpStatus.NOT_FOUND));

        Optional.ofNullable(userRequest.getEmail()).ifPresent(userEntity::setEmail);
        Optional.ofNullable(userRequest.getPickUpAddress()).ifPresent(userEntity::setPickUpAddress);
        Optional.ofNullable(userRequest.getDropOffAddress()).ifPresent(userEntity::setDropOffAddress);


        Optional.ofNullable(userRequest.getPhoneNumbers())
                .map(phoneNumberMapper::toDbo)
                .ifPresent(userEntity::setPhoneNumbers);

        UserEntity updatedUserEntity = userRepository.save(userEntity);
        UserResponse userResponse = userMapper.toDto(updatedUserEntity);
        return userResponse;
    }

    @Override
    @Transactional
    public UserResponse updateUserPassword(PasswordRequest passwordRequest) {
        UserEntity userEntity = userRepository.findById(tokenUser.userDetail().getId()).orElseThrow(() -> new GeneralException(TechExceptionCodes.USER_NOT_FOUND, HttpStatus.NOT_FOUND));
        boolean isMatch = new BCryptPasswordEncoder().matches(passwordRequest.getOldPassword(), userEntity.getPassword());
        if (isMatch) {
            userEntity.setPassword(new BCryptPasswordEncoder().encode(passwordRequest.getNewPassword()));
            userRepository.save(userEntity);
        } else {
            throw new GeneralException(TechExceptionCodes.INVALID_OLD_PASSWORD, HttpStatus.NOT_ACCEPTABLE);
        }
        return userMapper.toDto(userEntity);
    }
}
