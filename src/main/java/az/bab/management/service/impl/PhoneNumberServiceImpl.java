package az.bab.management.service.impl;

import az.bab.management.config.util.TokenUser;
import az.bab.management.exception.GeneralException;
import az.bab.management.exception.utils.TechExceptionCodes;
import az.bab.management.model.request.PhoneNumberRequest;
import az.bab.management.domain.PhoneNumberEntity;
import az.bab.management.domain.UserEntity;
import az.bab.management.mapper.PhoneNumberMapper;
import az.bab.management.mapper.UserMapper;
import az.bab.management.model.response.PhoneNumberResponse;
import az.bab.management.model.response.UserResponse;
import az.bab.management.repository.PhoneNumberRepository;
import az.bab.management.repository.UserRepository;
import az.bab.management.service.PhoneNumberService;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class PhoneNumberServiceImpl implements PhoneNumberService {

    @Autowired
    private PhoneNumberRepository phoneNumberRepository;

    @Autowired
    private PhoneNumberMapper phoneNumberMapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TokenUser tokenUser;

    @Override
    public PhoneNumberResponse createPhoneNumber(PhoneNumberRequest phoneNumberRequest) {
        UserEntity userEntity = userRepository.findById(tokenUser.userDetail().getId()).orElseThrow(() -> new GeneralException(TechExceptionCodes.USER_NOT_FOUND, HttpStatus.NOT_FOUND));
        PhoneNumberEntity phoneNumberEntity = phoneNumberMapper.toDbo(phoneNumberRequest);
        phoneNumberEntity.setUser(userEntity);
        PhoneNumberEntity savedPhoneNumberEntity = phoneNumberRepository.save(phoneNumberEntity);
        PhoneNumberResponse phoneNumberResponse = phoneNumberMapper.toDto(savedPhoneNumberEntity);
        return phoneNumberResponse;
    }

    @Override
    public PhoneNumberResponse updatePhoneNumber(Long phoneNumberId, PhoneNumberRequest phoneNumberRequest) {
        UserEntity userEntity = userRepository.findById(tokenUser.userDetail().getId()).orElseThrow(() -> new GeneralException(TechExceptionCodes.USER_NOT_FOUND, HttpStatus.NOT_FOUND));

        PhoneNumberEntity phoneNumberEntity = userEntity.getPhoneNumbers().stream()
                .filter(phoneNumbers -> phoneNumbers.getId().equals(phoneNumberId))
                .findFirst()
                .orElseThrow(() -> new GeneralException(TechExceptionCodes.PHONE_NUMBER_NOT_FOUND, HttpStatus.NOT_FOUND));

        phoneNumberEntity.setPhoneNumber(phoneNumberRequest.getPhoneNumber());

        phoneNumberRepository.save(phoneNumberEntity);
        PhoneNumberResponse updatedPhoneNumberResponse = phoneNumberMapper.toDto(phoneNumberEntity);

        return updatedPhoneNumberResponse;

    }

    @Override
    public PhoneNumberResponse updatePhoneNumber(Long userId, Long phoneNumberId, PhoneNumberRequest phoneNumberRequest) {
        UserEntity userEntity = userRepository.findById(userId).orElseThrow(() -> new GeneralException(TechExceptionCodes.USER_NOT_FOUND, HttpStatus.NOT_FOUND));

        PhoneNumberEntity phoneNumberEntity = userEntity.getPhoneNumbers().stream()
                .filter(phoneNumbers -> phoneNumbers.getId().equals(phoneNumberId))
                .findFirst()
                .orElseThrow(() -> new GeneralException(TechExceptionCodes.PHONE_NUMBER_NOT_FOUND, HttpStatus.NOT_FOUND));

        phoneNumberEntity.setPhoneNumber(phoneNumberRequest.getPhoneNumber());

        phoneNumberRepository.save(phoneNumberEntity);
        PhoneNumberResponse updatedPhoneNumberResponse = phoneNumberMapper.toDto(phoneNumberEntity);

        return updatedPhoneNumberResponse;

    }

    @Override
    @Transactional
    public void deletePhoneNumber(Long phoneNumberId) {
        UserEntity userEntity = userRepository.findById(tokenUser.userDetail().getId()).orElseThrow(() -> new GeneralException(TechExceptionCodes.USER_NOT_FOUND, HttpStatus.NOT_FOUND));

        PhoneNumberEntity phoneNumberEntity = userEntity.getPhoneNumbers().stream()
                .filter(phoneNumbers -> phoneNumbers.getId().equals(phoneNumberId))
                .findFirst()
                .orElseThrow(() -> new GeneralException(TechExceptionCodes.PHONE_NUMBER_NOT_FOUND, HttpStatus.NOT_FOUND));


        userEntity.getPhoneNumbers().remove(phoneNumberEntity);
        userRepository.save(userEntity);
        phoneNumberRepository.deleteById(phoneNumberId);


    }

    @Override
    @Transactional
    public void deletePhoneNumber(Long userId, Long phoneNumberId) {
        UserEntity userEntity = userRepository.findById(userId).orElseThrow(() -> new GeneralException(TechExceptionCodes.USER_NOT_FOUND, HttpStatus.NOT_FOUND));

        PhoneNumberEntity phoneNumberEntity = userEntity.getPhoneNumbers().stream()
                .filter(phoneNumbers -> phoneNumbers.getId().equals(phoneNumberId))
                .findFirst()
                .orElseThrow(() -> new GeneralException(TechExceptionCodes.PHONE_NUMBER_NOT_FOUND, HttpStatus.NOT_FOUND));

        userEntity.getPhoneNumbers().remove(phoneNumberEntity);
        userRepository.save(userEntity);
        phoneNumberRepository.deleteById(phoneNumberId);

    }
}
