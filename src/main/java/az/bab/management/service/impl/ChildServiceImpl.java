package az.bab.management.service.impl;

import az.bab.management.config.util.TokenUser;
import az.bab.management.domain.ChildEntity;
import az.bab.management.domain.PhoneNumberEntity;
import az.bab.management.domain.UserEntity;
import az.bab.management.exception.GeneralException;
import az.bab.management.exception.utils.TechExceptionCodes;
import az.bab.management.mapper.ChildMapper;
import az.bab.management.model.request.ChildRequest;
import az.bab.management.model.response.ChildResponse;
import az.bab.management.repository.ChildRepository;
import az.bab.management.repository.UserRepository;
import az.bab.management.service.ChildService;
import az.bab.management.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ChildServiceImpl implements ChildService {

    private static final Logger log = LoggerFactory.getLogger(ChildServiceImpl.class);
    @Autowired
    private ChildRepository childRepository;

    @Autowired
    private ChildMapper childMapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TokenUser tokenUser;

    @Override
    public List<ChildResponse> getAllUserChildren() {
        return childRepository.findByUser(tokenUser.userDetail())
                .stream()
                .map(childMapper::toDto)
                .toList();
    }


    @Override
    public List<ChildResponse> getAllUserChildren(Long userId) {
        List<ChildEntity> children = childRepository.findAllByUserId(userId);
        return children.stream()
                .map(childMapper::toDto)
                .toList();
    }

    @Override
    public List<ChildResponse> getAllChildren() {
        return childRepository.findAll()
                .stream()
                .map(childMapper::toDto)
                .toList();
    }

    @Override
    public ChildResponse createChild(ChildRequest childRequest) {
        UserEntity userEntity = userRepository.findById(tokenUser.userDetail().getId()).orElseThrow(() -> new GeneralException(TechExceptionCodes.USER_NOT_FOUND, HttpStatus.NOT_FOUND));
        ChildEntity childEntity = childMapper.toDbo(childRequest);
        childEntity.setUser(userEntity);
        ChildEntity savedChild = childRepository.save(childEntity);
        ChildResponse childResponse = childMapper.toDto(savedChild);

        return childResponse;
    }

    @Override
    public ChildResponse updateChildFields(Long id, ChildRequest childRequest) {
        ChildEntity childEntity = childRepository.findByIdAndUser(id, tokenUser.userDetail()).orElseThrow(() -> new GeneralException(TechExceptionCodes.CHILD_NOT_FOUND, HttpStatus.NOT_FOUND));
        Optional.ofNullable(childRequest.getName()).ifPresent(childEntity::setName);
        Optional.ofNullable(childRequest.getSurname()).ifPresent(childEntity::setSurname);
        ChildEntity updatedChildEntity = childRepository.save(childEntity);
        ChildResponse childResponse = childMapper.toDto(updatedChildEntity);

        return childResponse;
    }

    @Override
    public ChildResponse updateChildFields(Long userId, Long id, ChildRequest childRequest) {
        ChildEntity childEntity = childRepository.findByUserIdAndId(userId, id).orElseThrow(() -> new GeneralException(TechExceptionCodes.CHILD_NOT_FOUND, HttpStatus.NOT_FOUND));
        Optional.ofNullable(childRequest.getName()).ifPresent(childEntity::setName);
        Optional.ofNullable(childRequest.getSurname()).ifPresent(childEntity::setSurname);
        ChildEntity updatedChildEntity = childRepository.save(childEntity);
        ChildResponse childResponse = childMapper.toDto(updatedChildEntity);
        return childResponse;
    }


    @Override
    public void deleteChild(Long id) {
        ChildEntity childEntity = childRepository.findByIdAndUser(id, tokenUser.userDetail()).orElseThrow(() -> new GeneralException(TechExceptionCodes.CHILD_NOT_FOUND, HttpStatus.NOT_FOUND));
        childRepository.delete(childEntity);
    }

    @Override
    public void deleteChild(Long userId, Long id) {
        ChildEntity childEntity = childRepository.findByUserIdAndId(userId, id).orElseThrow(() -> new GeneralException(TechExceptionCodes.CHILD_NOT_FOUND, HttpStatus.NOT_FOUND));
        childRepository.delete(childEntity);
    }
}
