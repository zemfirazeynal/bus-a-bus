package az.bab.management.model.response;


import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SchoolResponse {

    private Long id;

    private String name;
    private String number;
    private String address;

//    private List<ChildEntity> children;

    private LocalDateTime createDate;
    private LocalDateTime updateDate;
}
