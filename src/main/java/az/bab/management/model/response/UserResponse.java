package az.bab.management.model.response;

import az.bab.management.model.request.PhoneNumberRequest;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse {
    private Long id;
    private String name;
    private String surname;
    private String username;
    private String email;
    private String pickUpAddress;
    private String dropOffAddress;
    private LocalDateTime createDate;
    private LocalDateTime updateDate;
    List<PhoneNumberResponse> phoneNumbers;
    private String provider;

}
