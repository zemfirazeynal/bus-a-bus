package az.bab.management.model.response;

import az.bab.management.model.request.PermissionRequest;
import az.bab.management.model.request.RoleRequest;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RolePermissionResponse {


    private RoleRequest role;
    private PermissionRequest permission;
    private LocalDateTime createDate;
    private LocalDateTime updateDate;

    // private List<UserRolePermissionEntity> userRolePermissionEntities;
}
