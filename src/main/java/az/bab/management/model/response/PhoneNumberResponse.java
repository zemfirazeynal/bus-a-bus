package az.bab.management.model.response;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PhoneNumberResponse {
    private Long id;
    private String phoneNumber;
    private Long userId;
    private LocalDateTime createDate;
    private LocalDateTime updateDate;
}
