package az.bab.management.model.response;


import az.bab.management.enums.BusinessStatus;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommonResponse<T> {


    @JsonProperty("code")
    private int code;

    @JsonProperty("message")
    private String message;

    @JsonProperty("data")
    private T data;

    public static <T> CommonResponse<T> instance(BusinessStatus businessStatusCodes) {
        return CommonResponse.<T>builder()
                .code(businessStatusCodes.getCode())
                .message(businessStatusCodes.getMessage())
                .build();
    }

    public static <T> CommonResponse<T> instance(T data, BusinessStatus businessStatusCodes) {
        return CommonResponse.<T>builder()
                .code(businessStatusCodes.getCode())
                .message(businessStatusCodes.getMessage())
                .data(data).build();
    }

    public static <T> CommonResponse<T> instance(T data, BusinessStatus businessStatusCodes, String message) {
        return CommonResponse.<T>builder()
                .code(businessStatusCodes.getCode())
                .message(businessStatusCodes.getMessage() + ":" + message)
                .data(data).build();
    }
}

