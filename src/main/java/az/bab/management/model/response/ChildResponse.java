package az.bab.management.model.response;

import az.bab.management.model.request.UserRequest;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChildResponse {

    private Long id;
    private String name;
    private String surname;

    private Long userId;
    private Long schoolId;

    private LocalDateTime createDate;
    private LocalDateTime updateDate;
}
