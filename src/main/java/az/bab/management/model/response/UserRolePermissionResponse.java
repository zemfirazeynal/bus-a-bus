package az.bab.management.model.response;

import az.bab.management.model.request.RolePermissionRequest;
import az.bab.management.model.request.UserRequest;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserRolePermissionResponse {

    private Long id;
    private UserRequest user;
    private RolePermissionRequest rolePermission;
    private LocalDateTime createDate;
    private LocalDateTime updateDate;

}
