package az.bab.management.model.response;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PermissionResponse {

    private Long id;
    private String name;
    private String description;

    private LocalDateTime createDate;
    private LocalDateTime updateDate;


//    private List<RolePermissionEntity> rolePermissionEntities;
}
