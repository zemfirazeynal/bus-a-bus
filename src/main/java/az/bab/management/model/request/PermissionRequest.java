package az.bab.management.model.request;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PermissionRequest {

    private String name;
    private String description;


//    private List<RolePermissionEntity> rolePermissionEntities;
}
