package az.bab.management.model.request;


import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PasswordRequest {
    private String oldPassword;
    private String newPassword;
}
