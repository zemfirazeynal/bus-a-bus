package az.bab.management.model.request;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SchoolRequest {

    private String name;
    private String number;
    private String address;


}
