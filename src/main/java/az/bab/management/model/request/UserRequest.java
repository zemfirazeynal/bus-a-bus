package az.bab.management.model.request;

import az.bab.management.validator.PasswordRegex;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserRequest {

    @NotBlank(message = "Ad boş olmamalıdır")
    private String name;

    @NotBlank(message = "Soyad boş olmamalıdır")
    private String surname;
    //    private String username;

    @NotBlank(message = "Email boş olmamalıdır")
    @Email
    private String email;

    @PasswordRegex(message = "Parol tələblərə cavab vermir: 10-20 simvol, ən azı bir kiçik hərf, bir böyük hərf və bir rəqəm olmalıdır")
    private String password;

    @NotBlank(message = "Götürmə ünvanı boş olmamalıdır")
    private String pickUpAddress;

    @NotBlank(message = "Düşmə ünvanı boş olmamalıdır")
    private String dropOffAddress;

    @Size(min = 1, message = "Ən azı bir telefon nömrəsi daxil edilməlidir")
    List<PhoneNumberRequest> phoneNumbers;
}
