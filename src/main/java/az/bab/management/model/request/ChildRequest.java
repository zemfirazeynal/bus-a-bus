package az.bab.management.model.request;

import az.bab.management.model.response.SchoolResponse;
import jakarta.validation.constraints.NotBlank;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChildRequest {
    @NotBlank(message = "Ad boş olmamalıdır")
    private String name;

    @NotBlank(message = "Soyad boş olmamalıdır")
    private String surname;

    private Long schoolId;

}
