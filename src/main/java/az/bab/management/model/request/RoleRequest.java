package az.bab.management.model.request;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoleRequest {

    private Long id;
    private String name;
    private String description;


    //private List<RolePermissionEntity> rolePermissionEntities;
}
