package az.bab.management.model.request;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RolePermissionRequest {


    private RoleRequest role;
    private PermissionRequest permission;


    // private List<UserRolePermissionEntity> userRolePermissionEntities;
}
