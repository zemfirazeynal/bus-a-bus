package az.bab.management.model.request;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserRolePermissionRequest {

    private Long id;
    private UserRequest user;
    private RolePermissionRequest rolePermission;

}
