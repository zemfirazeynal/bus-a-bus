package az.bab.management.controller;

import az.bab.management.domain.UserEntity;
import az.bab.management.service.BABService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/bob")
@PreAuthorize("hasRole('ADMIN') || hasRole('USER')")
public class BABController {

    @Autowired
    private BABService BABService;

    @GetMapping
    String login() {
        return "Hello TEMPLATE";
    }

    @GetMapping("/dashboard")
    @PreAuthorize("hasAuthority('ADMINx') || hasAuthority('USERx')")
    Principal dashboard(Principal principal) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        System.out.println("Authorities: " + authentication.getAuthorities());
        return principal;
    }

    @GetMapping("/all")
    @PreAuthorize("hasAuthority('ADMIN') || hasAuthority('USER')")
    List<UserEntity> allUser() {
        return BABService.findAll();
    }

}