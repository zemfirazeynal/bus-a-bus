package az.bab.management.controller;

import az.bab.management.model.request.PasswordRequest;
import az.bab.management.model.request.UserRequest;
import az.bab.management.model.response.UserResponse;
import az.bab.management.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/admin/all")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public List<UserResponse> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/getUserInfo")
    public ResponseEntity<UserResponse> getUserInfo() {
        UserResponse userResponse = userService.getUserInfo();
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(userResponse);
    }

    @GetMapping("/admin/id")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public ResponseEntity<?> getUserById(@RequestParam Long id) {
        UserResponse userResponse = userService.getUserById(id);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(userResponse);
    }

    @DeleteMapping("/admin/deleteUser/id")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public void deleteUser(@RequestParam Long id, UserRequest userRequest) {
        userService.deleteUser(id,userRequest);
    }

    @PatchMapping("/updateUserFields")
    public ResponseEntity<?> updateUserFields(@RequestBody UserRequest userRequest) {
        UserResponse userResponse = userService.updateUserFields(userRequest);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(userResponse);
    }

    @PatchMapping("/updateUserPassword")
    public ResponseEntity<?> updateUserPassword(@RequestBody PasswordRequest passwordRequest) {
        userService.updateUserPassword(passwordRequest);
        return ResponseEntity.ok("User's password updated successfully");
    }
}