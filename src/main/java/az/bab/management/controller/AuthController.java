package az.bab.management.controller;

import az.bab.management.config.util.JwtUtil;
import az.bab.management.enums.BusinessStatus;
import az.bab.management.exception.GeneralException;
import az.bab.management.exception.utils.TechExceptionCodes;
import az.bab.management.model.request.AuthenticationRequest;
import az.bab.management.model.request.EmailRequest;
import az.bab.management.model.response.AuthenticationResponse;
import az.bab.management.model.request.UserRequest;
import az.bab.management.model.response.CommonResponse;
import az.bab.management.service.AuthService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;


@RestController
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private AuthService authService;

    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@Valid @RequestBody UserRequest userRequest) {
        authService.registerUser(userRequest);
        return ResponseEntity.ok(CommonResponse.instance(userRequest, BusinessStatus.SUCCESS));
    }

    @PatchMapping("/updateGoogleUser")
    public ResponseEntity<?> updateGoogleUser(@RequestBody UserRequest userRequest) {
        authService.updateGoogleUser(userRequest);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@RequestBody AuthenticationRequest authRequest) {
        AuthenticationResponse authenticationResponse = authService.authenticateUser(authRequest);
        return ResponseEntity.ok(authenticationResponse);
    }

    @GetMapping("/forgotPassword")
    public ResponseEntity<?> forgotPassword(@RequestBody EmailRequest emailRequest) {
        return authService.forgotPassword(emailRequest);
    }

    @GetMapping("/reset-password/{token}/{password}")
    public ResponseEntity<?> resetPassword(@PathVariable String token, @PathVariable String password) {
        return authService.resetPassword(password, token);
    }
}