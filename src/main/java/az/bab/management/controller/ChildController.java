package az.bab.management.controller;


import az.bab.management.model.request.ChildRequest;
import az.bab.management.model.response.ChildResponse;
import az.bab.management.model.response.SchoolResponse;
import az.bab.management.service.ChildService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/children")
public class ChildController {

    @Autowired
    private ChildService childService;

    @GetMapping("/getAllUserChildren")
    public List<ChildResponse> getAllUserChildren() {
        return childService.getAllUserChildren();
    }

    @GetMapping("/admin/getAllUsersChildren/{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public List<ChildResponse> getAllUserChildren(@PathVariable ("id") Long userId) {
        return childService.getAllUserChildren(userId);
    }

    @GetMapping("/admin/getAllChildren")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public List<ChildResponse> getAllChildren() {
        return childService.getAllChildren();
    }

    @PostMapping("/createChild")
    public ResponseEntity<?> createChild(@RequestBody ChildRequest childRequest) {
        ChildResponse childResponse = childService.createChild(childRequest);
        return ResponseEntity.status(HttpStatus.CREATED).body(childResponse);
    }

    @PatchMapping("/updateChildFields/{id}")
    public ResponseEntity<?> updateChildFields(@PathVariable("id") Long id, @RequestBody ChildRequest childRequest) {
        childService.updateChildFields(id, childRequest);
        return ResponseEntity.ok("Child updated successfully");
    }

    @PatchMapping("/admin/updateChildFields/{userId}/{childId}")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public ResponseEntity<?> updateChildFields(@PathVariable("userId") Long userId, @PathVariable("childId") Long id, @RequestBody ChildRequest childRequest) {
        childService.updateChildFields(userId, id, childRequest);
        return ResponseEntity.ok("Child updated successfully");
    }

    @DeleteMapping("/deleteChild/{id}")
    public void deleteChild(@PathVariable("id") Long id) {
        childService.deleteChild(id);
    }

    @DeleteMapping("/admin/deleteChild/{userId}/{childId}")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public void deleteChild(@PathVariable("userId") Long userId, @PathVariable("childId") Long id) {
        childService.deleteChild(userId,id);
    }
}
