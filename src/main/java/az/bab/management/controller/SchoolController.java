package az.bab.management.controller;


import az.bab.management.model.request.SchoolRequest;
import az.bab.management.model.request.UserRequest;
import az.bab.management.model.response.SchoolResponse;
import az.bab.management.service.SchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/schools")
public class SchoolController {

    @Autowired
    private SchoolService schoolService;

    @PostMapping("/admin/createSchool")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public ResponseEntity<?> createSchool(@RequestBody SchoolRequest schoolRequest) {
         SchoolResponse schoolResponse = schoolService.createSchool(schoolRequest);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(schoolResponse);
    }

    @GetMapping("/getAllSchools")
    public List<SchoolResponse> getAllSchools() {
        return schoolService.getAllSchools();
    }

    @PatchMapping("/admin/updateSchoolFields/{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public ResponseEntity<?> updateSchoolFields(@PathVariable("id") Long id, @RequestBody SchoolRequest schoolRequest) {
        SchoolResponse schoolResponse = schoolService.updateSchoolFields(id, schoolRequest);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(schoolResponse);
    }

    @DeleteMapping("/admin/deleteSchool/{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public void deleteSchool(@PathVariable("id") Long id, SchoolRequest schoolRequest) {
        schoolService.deleteSchool(id, schoolRequest);
    }
}
