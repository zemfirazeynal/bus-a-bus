package az.bab.management.controller;


import az.bab.management.model.request.PhoneNumberRequest;
import az.bab.management.model.response.PhoneNumberResponse;
import az.bab.management.service.PhoneNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.awt.print.PrinterJob;

@RestController
@RequestMapping("/phoneNumbers")

public class PhoneNumberController {

    @Autowired
    private PhoneNumberService phoneNumberService;

    @PostMapping("/createPhoneNumber")
    public ResponseEntity<?> createPhoneNumber(@RequestBody PhoneNumberRequest phoneNumberRequest) {
        PhoneNumberResponse phoneNumberResponse = phoneNumberService.createPhoneNumber(phoneNumberRequest);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(phoneNumberResponse);
    }

    @PatchMapping("/updatePhoneNumber/{phoneNumberId}")
    public ResponseEntity<?> updatePhoneNumber(@PathVariable("phoneNumberId") Long phoneNumberId,
                                               @RequestBody PhoneNumberRequest phoneNumberRequest) {
         PhoneNumberResponse phoneNumberResponse = phoneNumberService.updatePhoneNumber(phoneNumberId, phoneNumberRequest);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(phoneNumberResponse);
    }

    @PatchMapping("/admin/updatePhoneNumber/{userId}/{phoneNumberId}")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public ResponseEntity<?> updatePhoneNumber(@PathVariable Long userId, @PathVariable Long phoneNumberId,
                                               @RequestBody PhoneNumberRequest phoneNumberRequest) {
        PhoneNumberResponse phoneNumberResponse = phoneNumberService.updatePhoneNumber(userId, phoneNumberId, phoneNumberRequest);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(phoneNumberResponse);
    }

    @DeleteMapping("/deletePhoneNumber/{phoneNumberId}")
    public void deletePhoneNumber(@PathVariable("phoneNumberId") Long phoneNumberId) {
        phoneNumberService.deletePhoneNumber(phoneNumberId);
    }

    @DeleteMapping("/admin/deletePhoneNumber/{userId}/{phoneNumberId}")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public void deletePhoneNumber(@PathVariable("userId") Long userId, @PathVariable("phoneNumberId") Long phoneNumberId) {
        phoneNumberService.deletePhoneNumber(userId,phoneNumberId);
    }

}
