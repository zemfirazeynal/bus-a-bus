FROM openjdk:17-jdk-slim

# Set the working directory in the container
WORKDIR /app

# Copy the Gradle build output JAR file to the container
COPY build/libs/bus-a-bus-0.1.0.jar app.jar

# Expose the port that the app runs on
EXPOSE 8080

# Run the Spring Boot application
ENTRYPOINT ["java", "-jar", "app.jar"]